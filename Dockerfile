# syntax=docker/dockerfile:1

FROM node:14
ENV NODE_ENV=production

WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
RUN npm install --save-dev chromedriver
RUN npm i --save-dev @wdio/local-runner@latest @wdio/cucumber-framework@latest @wdio/selenium-standalone-service@latest
RUN npm install @wdio/cli
RUN npm install @wdio/local-runner
RUN npm install wdio-docker-service --save-dev


# Bundle app source
COPY . .

# Your app binds to port 8080 so you'll use the EXPOSE 
# instruction to have it mapped by the docker daemon.
EXPOSE 4444

# tells Docker what command we want to run when our image 
# is run inside of a container.
CMD [ "npm", "test" ]