This project uses Cucumber.js for feature files to test the ToDo app (https://todomvc.com/examples/react/#/),
WebdriverIO is utilized for chrome browser automation and even makes using selenium-standalone easy through
it's services which tie in with Docker.

BDD testing methodology was used, with the following behaviors being tested:
1. Add and Destory ToDo's
2. Set ToDo's from active to complete by toggling
3. Filter ToDo's based on status or all
4. Toggle all ToDo's to complete
5. Clear all completed ToDo's

In Addition a test was created to validate the above functionality with test data
from the jsonplaceholder API in the feature file: apiTests.feature 
