Feature: Filter items in the ToDo list

    """
    This feature tests the filter options on the todos page.
    """

    Scenario: User handles the ToDo list filters
        Given the user has added "laundry" and "dishes" to the ToDo list
        And the user has completed their laundry
        When the user clicks the active filter
        Then only the active task is visible
        But the completed task is not visible
        When the user clicks the completed filter
        Then only the completed task is visible
        But the active task is not visible
        When the user clicks the all filter
        Then both the active and completed task are visible