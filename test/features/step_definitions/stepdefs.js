require('chromedriver');
const { Given, When, Then, } = require("@cucumber/cucumber");
let todo = require('../../Pages/todo');


// Edit Items Feature
Given('the user comtemplates whether or not to do their {string}', async (task) => {
    this.task = task;
});
When('the user types laundry into the what needs to be done? input', async () => {
    await todo.addToDoItem(this.task);
});
Then('laundry is visible in the ToDo list', async () => {
    await todo.checkItemIsDisplayedOrNot(this.task, true);
});
When('the user clicks the destroy button', async () => {
    await browser.pause(1000);
    await todo.destroyToDoItem(this.task);
});
Then('laundry is not visible in the ToDo list', async () => {
    await todo.checkItemIsDisplayedOrNot(this.task, false);
});


// Filter Items Feature
Given('the user has added {string} and {string} to the ToDo list', async (task1, task2) => {
    this.completedTask = task1;
    this.activeTask = task2;
    await todo.addToDoItem(this.completedTask);
    await todo.addToDoItem(this.activeTask);
});
Given('the user has completed their laundry', async () => {
    console.log("The user does their laundry.");
    await todo.toggleToDoItem(this.completedTask);
    await todo.checkItemIsSelectedOrNot(this.completedTask, true);
    await todo.checkItemsLeft(1);
    await todo.checkButtonIsDisplayedOrNot("clear-completed", true);
});
When('the user clicks the active filter', async () => {
    await todo.filterActiveToDoItems();
});
Then('only the active task is visible', async () => {
    await todo.checkItemIsDisplayedOrNot(this.activeTask, true);
});
Then('the completed task is not visible', async () => {
    await todo.checkItemIsDisplayedOrNot(this.completedTask, false);
});
When('the user clicks the completed filter', async () => {
    await todo.filterCompletedToDoItems();
});
Then('only the completed task is visible', async () => {
    await todo.checkItemIsDisplayedOrNot(this.completedTask, true);
});
Then('the active task is not visible', async () => {
    await todo.checkItemIsDisplayedOrNot(this.activeTask, false);
});
When('the user clicks the all filter', async () => {
    await todo.filterAllToDoItems();
});
Then('both the active and completed task are visible', async () => {
    await todo.checkItemIsDisplayedOrNot(this.activeTask, true);
    await todo.checkItemIsDisplayedOrNot(this.completedTask, true);
});


// Complete Items Feature
Given('the user has added {string} to the ToDo list', async (tasks) => {
    this.task = tasks;
    await todo.addToDoItem(this.task);
});
When('the user has completed their task and toggles the element', async () => {
    await todo.toggleToDoItem(this.task);
    await todo.checkItemsLeft(0);
    await todo.checkButtonIsDisplayedOrNot("clear-completed", true);
});
Then('the task is {string}', async (checked) => {
    this.check = checked;
    await todo.checkItemIsSelectedOrNot(this.task, this.check);
});


// Clear Completed Items Feature
Given('the user has completed their ToDo list', async function (tasks) {
    this.tasks = tasks;
    await todo.completeToDoList(tasks);
});
When('the user clicks the clear completed button', async () => {
    await todo.clearCompletedItems();
});
Then('all tasks are removed from the ToDo list', async () => {
    toDoList = await todo.buildToDoList(this.tasks);
    await todo.checkItemsExistOrNot(toDoList, false);
});


// Toggle All Feature
Given('the user has completed all their ToDo list items', async function (tasks) {
    this.tasks = tasks;
    await todo.addToDoItems(tasks);
});
When('the user clicks the toggle all element', async () => {
    await todo.toggleAllToDoItems();
});
Then('all ToDo items are check-marked and completed', async () => {
    this.toDoList = await todo.buildToDoList(this.tasks);
    await todo.checkItemsAreSelectedOrNot(this.toDoList, true);
});
When('the user clicks the toggle all element again', async () => {
    await todo.toggleAllToDoItems();
});
Then('all ToDo items are unchecked and active', async () => {
    await todo.checkItemsAreSelectedOrNot(this.toDoList, false);
});


// apiTest Feature
Given('the data has been fetched with the API', async () => {
    arr = await todo.getDataFromAPI(1);
    this.map = await todo.buildMap(arr);
});
When('the user adds their todo titles', async () => {
    await todo.addTodosFromMap(this.map);
});
Then('the users todos are visible', async () => {
    arr = await todo.buildArraysOfToDos(this.map);
    this.allTasks = arr[0];
    this.activeTasks = arr[1];
    this.completedTasks = arr[3];
    await todo.checkItemsAreDisplayedOrNot(this.allTasks, true);
});
When('the user toggles the todos based on completion status', async () => {
    await todo.toggleToDosFromMap(this.map);
});
Then('the todos are marked as completed', async () => {
    await todo.checkItemsAreSelectedOrNot(this.completedTasks, true);
});
When('the user clicks on the active filter', async () => {
    await todo.filterActiveToDoItems();
});
Then('only the active todos are shown', async () => {
    await todo.checkItemsAreDisplayedOrNot(this.activeTasks, true);
});
When('the user clicks on the completed filter', async () => {
    await todo.filterCompletedToDoItems();
});
Then('only the completed todos are shown', async () => {
    await todo.checkItemsAreDisplayedOrNot(this.completedTasks, true);
});
When('the user clicks on the all filter', async () => {
    await todo.filterAllToDoItems();
});
Then('all todos are shown', async () => {
    await todo.checkItemsAreDisplayedOrNot(this.allTasks, true);
});
Then('the completed todos are removed', async () => {
    await browser.pause(3000);
    await todo.clearCompletedItems();
});
When('the user toggles all remaining todos', async () => {
    await todo.toggleAllToDoItems();
});
Then('the remaining active todos are marked as completed', async () => {
    await todo.checkItemsAreSelectedOrNot(this.activeTasks, true);
});
When('the user clears the last completed todos', async () => {
    await todo.clearCompletedItems();
});
Then('the last completed todos are removed', async () => {
    await todo.checkItemsAreDisplayedOrNot(this.activeTasks, false);
});