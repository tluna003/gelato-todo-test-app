Feature: Complete items in the ToDo list

    """
    This feature tests the toggle ability of the labels input by the user.
    If a task has been completed user should be able to mark it as
    completed, and the label element will show a check mark.
    """

    Scenario Outline: User completes an item in the ToDo list
        Given the user has added "<task>" to the ToDo list
        When the user has completed their task and toggles the element
        Then the task is "<checked>"

        Examples:
            | task      | checked |
            | laundry   | true    |
            | dishes    | true    |
            | trash     | true    |
            | groceries | true    |
            |           | false   |