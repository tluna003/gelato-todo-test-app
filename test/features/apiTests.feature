Feature: Test ToDo App's Critical Functionality with Data from API

    """
    This feature runs through all the app's critical functionality
    in a similar way to the previous feature files, the difference being
    that this feature will use test data retreived from the jsonPlaceHolder API.
    """

    Scenario: User does it all with data from API
        Given the data has been fetched with the API
        When the user adds their todo titles
        Then the users todos are visible
        When the user toggles the todos based on completion status
        Then the todos are marked as completed
        When the user clicks on the active filter
        Then only the active todos are shown
        When the user clicks on the completed filter
        Then only the completed todos are shown
        When the user clicks on the all filter
        Then all todos are shown
        When the user clicks the clear completed button
        Then the completed todos are removed
        When the user toggles all remaining todos
        Then the remaining active todos are marked as completed
        When the user clears the last completed todos
        Then the last completed todos are removed


