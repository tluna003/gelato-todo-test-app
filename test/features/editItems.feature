Feature: Edit items in to ToDo list
    
    """
    This feature tests the todos app by adding tasks with the input box
    and removing tasks with the destory button (red x mark).
    """
    
    Scenario: User adds and removes a task from the ToDo list
        Given the user comtemplates whether or not to do their "laundry"
        When the user types laundry into the what needs to be done? input
        Then laundry is visible in the ToDo list
        When the user clicks the destroy button
        Then laundry is not visible in the ToDo list