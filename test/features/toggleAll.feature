Feature: Toggle all ToDo items as completed

    """
    This feature tests the toggle all label which will set all todos
    as completed.
    """

    Scenario: User toggles all ToDo items
        Given the user has completed all their ToDo list items
            | laundry   | 
            | dishes    |
            | trash     |
            | groceries |
        When the user clicks the toggle all element
        Then all ToDo items are check-marked and completed
        When the user clicks the toggle all element again
        Then all ToDo items are unchecked and active

