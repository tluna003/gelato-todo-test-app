Feature: Clear completed items in ToDo list

    """
    This feature tests the clear-completed button.
    When clicked all tasks marked as completed should be
    removed from the todos list.
    """

    Scenario: User clears completed items in ToDo list
        Given the user has completed their ToDo list
            | laundry   | 
            | dishes    |
            | trash     |
            | groceries |
        When the user clicks the clear completed button
        Then all tasks are removed from the ToDo list