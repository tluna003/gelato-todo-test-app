const assert = require('assert');
const fetch = require('node-fetch');
let elem;
let isDisplayed;
let isExisting;
let isSelected;
let isClickable;
let toDoList = [];

module.exports = {
    addToDoItem: async (task) => {
        console.log("addToDoItem Method");
        try {
            elem = await $("//input[@class='new-todo']");
            isDisplayed = await elem.isDisplayed();
            if (isDisplayed == true) {
                await elem.setValue(task);
                await browser.keys("\uE007");
            } else {
                assert.fail('The input box is not visible, or it is not interactable.');
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    addToDoItems: async (tasks) => {
        console.log("addToDoItems Method");
        try {
            toDoList = await module.exports.buildToDoList(tasks);
            // Adds tasks from toDoList array into ToDo list
            for (let i = 0; i < toDoList.length; i++) {
                await module.exports.addToDoItem(toDoList[i]);
                await module.exports.checkItemsLeft(i + 1);
                await module.exports.checkItemIsDisplayedOrNot(toDoList[i], true);
            }
            await module.exports.checkItemsLeft(toDoList.length);
        } catch (err) {
            console.log(err.stack);
        }
    },
    buildToDoList: async (tasks) => {
        console.log("buildToDoList Method");
        try {
            todo = tasks.raw();
            // Builds ToDo List Array from Feature File Data Table
            for (let i = 0; i < todo.length; i++) {
                toDoList.push(todo[i][0]);
                console.log(toDoList);
            }
        } catch (err) {
            console.log(err.stack);
        }
        return toDoList;
    },
    destroyToDoItem: async (task) => {
        console.log("destroyToDoItem Method");
        try {
            // Click on label element to show the destroy button.
            elem = await $("//label[text()='" + task + "']");
            isDisplayed = await elem.isDisplayed();
            if (isDisplayed == true) {
                elem.click();
            } else {
                assert.fail('The ' + task + ' element is not visible.');
            }
            // Now the destroy element should be visible.
            elem = await $("//label[text()='" + task + "']//following::button[1]");
            isDisplayed = await elem.isDisplayed();
            // Destroy
            if (isDisplayed == true) {
                elem.click();
            } else {
                assert.fail('The destroy element is not visible.');
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    checkItemIsDisplayedOrNot: async (task, visible) => {
        console.log("checkItemIsDisplayedOrNot Method");
        try {
            elem = await $("//label[text()='" + task + "']");
            isDisplayed = await elem.isDisplayed();
            if (isDisplayed == visible) {
                console.log(task + " element is " + visible + " in ToDo list.");
            } else {
                assert.fail(task + " element is not " + visible + " in ToDo list.");
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    checkItemsAreDisplayedOrNot: async (tasks, visible) => {
        console.log("checkItemsAreDisplayedOrNot Method");
        try {
            //toDoList = await module.exports.buildToDoList(tasks);
            for (i = 0; i < toDoList.length; i++) {
                await module.exports.checkItemIsDisplayedOrNot(toDoList[i], visible);
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    checkItemExistsOrNot: async (task, state) => {
        console.log("checkItemExistsOrNot Method");
        try {
            elem = await $("//label[text()='" + task + "']");
            isExisting = await elem.isExisting();
            if (isExisting == state) {
                console.log(task + ' element is ' + state + ' in DOM.');
            } else {
                assert.fail(task + ' element is not ' + state + ' in DOM.');
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    checkItemsExistOrNot: async (tasks, state) => {
        console.log("checkItemsExistOrNot Method");
        try {
            //toDoList = await module.exports.buildToDoList(tasks);
            for (i = 0; i < toDoList.length; i++) {
                await module.exports.checkItemExistsOrNot(toDoList[i], state);
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    toggleToDoItem: async (task) => {
        console.log("toggleToDoItem Method");
        try {
            elem = await $("//label[text()='" + task + "']//preceding::input[1]");
            isExisting = await elem.isExisting();
            if (isExisting == true) {
                elem.click();
            } else {
                assert.fail(task + ' element is not clickable.');
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    toggleAllToDoItems: async () => {
        console.log("toggleAllToDoItems Method");
        try {
            elem = await $("//label[@for='toggle-all']");
            isClickable = await elem.isClickable();
            if (isClickable == true) {
                elem.click();
            } else {
                assert.fail('The toggle-all element is not clickable.');
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    checkItemIsSelectedOrNot: async (task, state) => {
        console.log("checkItemIsSelectedOrNot Method");
        try {
            elem = await $("//label[text()='" + task + "']//preceding::input[1]");
            isSelected = await elem.isSelected();
            if (isSelected == state) {
                console.log(task + ' element is selected.');
            } else {
                assert.fail(task + ' element is not selected.');
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    checkItemsAreSelectedOrNot: async (tasks, state) => {
        console.log("checkIfItemsAreSelectedOrNot Method");
        try {
            //toDoList = await module.exports.buildToDoList(tasks);
            for (i = 0; i < toDoList.length; i++) {
                await module.exports.checkItemIsSelectedOrNot(toDoList[i], state);
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    checkItemsLeft: async (num) => {
        console.log("checkItemsLeft Method");
        try {
            elem = await $("//strong[@data-reactid='.0.2.0.0']");
            const value = await elem.getText();
            if (value.toString() == num.toString()) {
                console.log('The number of items left is correct.');
            } else {
                assert.fail('The number of items left is not correct.');
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    checkButtonIsDisplayedOrNot: async (button, visible) => {
        console.log("checkButtonIsDisplayedOrNot Method");
        try {
            elem = await $("//button[@class='" + button + "']");
            isDisplayed = await elem.isDisplayed();
            if (isDisplayed == visible) {
                console.log('The button is ' + visible + '.');
            } else {
                assert.fail('The button is not ' + visible + '.');
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    filterActiveToDoItems: async () => {
        console.log("filterActiveToDoItems Method");
        try {
            elem = await $("//a[@href='#/active']");
            isDisplayed = await elem.isDisplayed();
            if (isDisplayed == true) {
                elem.click();
            } else {
                assert.fail('The active filter is not visible.');
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    filterCompletedToDoItems: async () => {
        console.log("filterCompletedToDoItems Method");
        try {
            elem = await $("//a[@href='#/completed']");
            isDisplayed = await elem.isDisplayed();
            if (isDisplayed == true) {
                elem.click();
            } else {
                assert.fail('The completed filter is not visible.');
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    filterAllToDoItems: async () => {
        console.log("filterAllToDoItems Method");
        try {
            elem = await $("//a[@href='#/']");
            isDisplayed = await elem.isDisplayed();
            if (isDisplayed == true) {
                elem.click();
            } else {
                assert.fail('The all filter is not visible.');
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    clearCompletedItems: async () => {
        console.log("clearCompletedItems Method");
        try {
            elem = await $("//button[@class='clear-completed']");
            await elem.scrollIntoView();
            isDisplayed = await elem.isDisplayed();
            if (isDisplayed == true) {
                elem.click();
            } else {
                assert.fail('The clear-completed button is not visible.');
            }
        } catch (err) {
            console.log(err.stack);
        }
    },
    completeToDoList: async (tasks) => {
        console.log("completeToDoList Method");
        try {
            toDoList = await module.exports.buildToDoList(tasks);
            console.log(toDoList);
            await module.exports.addToDoItems(toDoList);
            // Check marks all items in toDoList
            for (let i = 0; i < toDoList.length; i++) {
                await module.exports.toggleToDoItem(toDoList[i]);
                await module.exports.checkItemsLeft(toDoList.length - (i + 1));
                await module.exports.checkItemIsSelectedOrNot(toDoList[i], true);
            }
            await module.exports.checkButtonIsDisplayedOrNot("clear-completed", true);
        } catch (err) {
            console.log(err.stack);
        }
    },
    // Pass a number value (UserId) to get data for that user.
    getDataFromAPI: async (userId) => {
        const response = await fetch('https://jsonplaceholder.typicode.com/users/' + userId + '/todos');
        const myJson = await response.json(); //extract JSON from the http response
        //const arr = JSON.parse(myJson);
        return myJson;
    },
    buildMap: async (arr) => {
        const todoMap = new Map();
        for (let i = 0; i < arr.length; i++) {
            const obj = arr[i];
            const key = obj.id;
            const title = obj.title;
            const completed = obj.completed;
            const value = [title, completed];
            todoMap.set(key, value);
        }
        console.log(todoMap);
        return todoMap;
    },
    buildArraysOfToDos: async (map) => {
        allTasks = [];
        activeTasks = [];
        completedTasks = [];
        for (let i = 0; i < map.size; i++) {
            arr = map.get(i+1);
            let task = arr[0];
            let state = arr[1];
            allTasks.push(task);
            if(state == false) {
                activeTasks.push(task);
            } else if (state == true) {
                completedTasks.push(task);
            }
        }
        masterArray = [allTasks, activeTasks, completedTasks];
        return masterArray;
    },
    addTodosFromMap: async (map) => {
        for (let i = 0; i < map.size; i++) {
            arr = map.get(i+1);
            let task = arr[0];
            if (task != null) {
                await module.exports.addToDoItem(task);
            } else {
                assert.fail('the argument is empty.');
            }
        }
    },
    toggleToDosFromMap: async (map) => {
        for (let i = 0; i < map.size; i++) {
            arr = map.get(i+1);
            let task = arr[0];
            let status = arr[1];
            if (status == true) {
                await module.exports.toggleToDoItem(task);
            } else {
                console.log('Status is false, so task has not been completed.');
            }
        }
    },
    
};